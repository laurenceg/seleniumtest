package com.zenoradio.api.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.junit.Test;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.zenoradio.api.Constant;

import junit.framework.Assert;

public class MultipleOnAirOffAirTest extends Base {

  @Test
  public void testValidMultipleLogin()
      throws ClientProtocolException, IOException, InterruptedException {
    System.out.println(">Login and Go on air and Off Air - version 1: Loop "
        + Constant.REPEAT_COUNT_FIVE + " times");
    // GIVEN
    // WHEN
    for (int i = 0; i < Constant.REPEAT_COUNT_FIVE; i++) {
      login(true);
      Thread.sleep(Constant.TEN_SEC_DELAY);
      goOnAir();
      Thread.sleep(Constant.TEN_SEC_DELAY);

      goOffAir();
      Thread.sleep(Constant.TEN_SEC_DELAY);
      logOut();
      Thread.sleep(Constant.TEN_SEC_DELAY);
    }
    getHtmlUnitDriver().close();
    System.out.println();
  }

  @Test
  public void testValidSingleLoginInvalidOnAir()
      throws ClientProtocolException, IOException, InterruptedException {
    System.out.println(">Login and Go on air and Off Air- version 2: Loop "
        + Constant.REPEAT_COUNT_FIVE + " times");

    login(true);
    Thread.sleep(Constant.TEN_SEC_DELAY);
    goOnAir();
    Thread.sleep(Constant.TEN_SEC_DELAY);
    goOffAir();
    Thread.sleep(Constant.TEN_SEC_DELAY);
    logOut();
    Thread.sleep(Constant.TEN_SEC_DELAY);

    System.out.println(">When user Logged Off and Goes On Multiple (" + Constant.REPEAT_COUNT_FIVE
        + ") times. Then it  should Not be successful.");
    logOut();
    for (int i = 0; i < Constant.REPEAT_COUNT_FIVE; i++) {
      goOnAirInvalid();
      Thread.sleep(Constant.TEN_SEC_DELAY);
      goOffAirInvalid(getHtmlUnitDriver());
      Thread.sleep(Constant.TEN_SEC_DELAY);
    }

    System.out.println();
  }

  @Test
  public void testValidSingleLoginInvalidOnAirLoadTest()
      throws ClientProtocolException, IOException, InterruptedException {
    System.out.println(">Login and Go on air and Off Air- version 3 (Crazy mode!)");

    login(true);
    Thread.sleep(Constant.ONE_SEC_DELAY);
    goOnAir();
    Thread.sleep(Constant.ONE_SEC_DELAY);
    goOffAir();
    Thread.sleep(Constant.ONE_SEC_DELAY);
    logOut();
    Thread.sleep(Constant.ONE_SEC_DELAY);

    System.out.println(">When user Logged Off and Goes On Multiple (" + Constant.REPEAT_COUNT_1K
        + ") times. Then it  should Not be successful.");
    logOut();
    login(true);
    for (int i = 0; i < Constant.REPEAT_COUNT_1K; i++) {
      goOnAir();
      Thread.sleep(Constant.ONE_SEC_DELAY);
      goOffAir();
      Thread.sleep(Constant.ONE_SEC_DELAY);
    }

    System.out.println();
  }


  private void goOnAir() {
    // WHEN
    getHtmlUnitDriver().get(Constant.ON_AIR_URL);
    System.out.println("Request     : " + Constant.ON_AIR_URL);
    String onAirJsonResponse = getHtmlUnitDriver().getPageSource();
    System.out.println("Response    :" + onAirJsonResponse);
    // THEN
    Assert.assertNotNull(onAirJsonResponse);
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_STAT_OK));
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_ON_AIR_STR));
  }

  private void goOnAirInvalid() {
    // WHEN
    getHtmlUnitDriver().get(Constant.ON_AIR_URL);
    System.out.println("Request     : " + Constant.ON_AIR_URL);
    String onAirJsonResponse = getHtmlUnitDriver().getPageSource();
    System.out.println("Response    :" + onAirJsonResponse);
    // THEN
    Assert.assertNotNull(onAirJsonResponse);
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_STAT_FAIL));
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_KEY_INVALID_LOGIN));
  }

  private void goOffAirInvalid(HtmlUnitDriver unitDriver) {
    // WHEN
    unitDriver.get(Constant.OFF_AIR_URL);
    System.out.println("Request     : " + Constant.OFF_AIR_URL);
    String onAirJsonResponse = unitDriver.getPageSource();
    System.out.println("Response    :" + onAirJsonResponse);
    // THEN
    Assert.assertNotNull(onAirJsonResponse);
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_STAT_FAIL));
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_KEY_INVALID_LOGIN));
  }


  private void goOffAir() {
    // WHEN
    getHtmlUnitDriver().get(Constant.OFF_AIR_URL);
    System.out.println("Request     : " + Constant.OFF_AIR_URL);
    String onAirJsonResponse = getHtmlUnitDriver().getPageSource();
    System.out.println("Response    :" + onAirJsonResponse);
    // THEN
    Assert.assertNotNull(onAirJsonResponse);
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_STAT_OK));
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_OFF_AIR_STR));
  }

}
