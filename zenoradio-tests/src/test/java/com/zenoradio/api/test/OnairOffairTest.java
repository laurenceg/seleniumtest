package com.zenoradio.api.test;


import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.junit.Test;

import com.zenoradio.api.Constant;

import junit.framework.Assert;

public class OnairOffairTest extends Base {


  @Test
  public void testGoOnAir() throws ClientProtocolException, IOException {
    System.out.println(">When authorized user go On-Air. It should be successful.");
    // GIVEN
    login(true);

    // WHEN
    String onAirJsonResponse = retrieveJSONResponse(Constant.ON_AIR_URL);

    // THEN
    Assert.assertNotNull(onAirJsonResponse);
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_STAT_OK));
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_ON_AIR_STR));
    System.out.println();
  }

  @Test
  public void testGoOnAirWithoutLoggingIn() throws ClientProtocolException, IOException {
    System.out
        .println(">When authorized user go On-Air without logging in. It should be Unsuccessful.");
    // GIVEN
    // WHEN
    String onAirJsonResponse = retrieveJSONResponse(Constant.ON_AIR_URL);

    // THEN
    Assert.assertNotNull(onAirJsonResponse);
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_STAT_FAIL));
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_KEY_INVALID_LOGIN));
    System.out.println();
  }

  @Test
  public void testInvalidUserGoOnAir() throws ClientProtocolException, IOException {
    System.out.println(">When Unauthorized user go On-Air. It should be unsuccessful.");
    // GIVEN
    login(false);

    // WHEN
    String onAirJsonResponse = retrieveJSONResponse(Constant.ON_AIR_URL);

    // THEN
    Assert.assertNotNull(onAirJsonResponse);
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_STAT_FAIL));
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_KEY_INVALID_LOGIN));
    System.out.println();
  }


  @Test
  public void testGoOffAir() throws ClientProtocolException, IOException {
    System.out.println(">When authorized user go Off-Air. It should be successful.");
    // GIVEN
    login(true);

    // WHEN
    String offAirJsonResponse = retrieveJSONResponse(Constant.OFF_AIR_URL);

    // THEN
    Assert.assertNotNull(offAirJsonResponse);
    assertEquals(true, offAirJsonResponse.contains(Constant.IS_STAT_OK));
    assertEquals(true, offAirJsonResponse.contains(Constant.IS_OFF_AIR_STR));
    System.out.println();
  }

  @Test
  public void testGoOffAirWithoutLoggingIn() throws ClientProtocolException, IOException {
    System.out
        .println(">When authorized user go Off-Air without logging In. It should be Unsuccessful.");
    // GIVEN
    // WHEN
    String onAirJsonResponse = retrieveJSONResponse(Constant.OFF_AIR_URL);

    // THEN
    Assert.assertNotNull(onAirJsonResponse);
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_STAT_FAIL));
    assertEquals(true, onAirJsonResponse.contains(Constant.IS_KEY_INVALID_LOGIN));
    System.out.println();
  }

  @Test
  public void testInvalidUserGoOffAir() throws ClientProtocolException, IOException {
    System.out.println(">When Unauthorized user go Off-Air. It should be unsuccessful.");
    // GIVEN
    login(false);

    // WHEN
    String offAirJsonResponse = retrieveJSONResponse(Constant.OFF_AIR_URL);

    // THEN
    Assert.assertNotNull(offAirJsonResponse);
    assertEquals(true, offAirJsonResponse.contains(Constant.IS_STAT_FAIL));
    assertEquals(true, offAirJsonResponse.contains(Constant.IS_KEY_INVALID_LOGIN));
    System.out.println();
  }

  private String retrieveJSONResponse(String url) {
    System.out.println("URL         : : " + url);
    getHtmlUnitDriver().get(url);

    String jsonReponse = getHtmlUnitDriver().getPageSource();
    System.out.println("Response    :" + jsonReponse);

    return jsonReponse;
  }



}
