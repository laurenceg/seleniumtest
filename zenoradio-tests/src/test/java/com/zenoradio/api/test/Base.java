package com.zenoradio.api.test;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.zenoradio.api.Constant;

public class Base {
  private static HtmlUnitDriver unitDriver;

  @Before
  public void setUp() {
    unitDriver = new HtmlUnitDriver(BrowserVersion.CHROME);
    unitDriver.setJavascriptEnabled(true);
  }

  @After
  public void tearDown() {
    unitDriver.quit();
  }

  protected HtmlUnitDriver getHtmlUnitDriver() {
    return unitDriver;
  }

  protected HtmlUnitDriver login(boolean isValid) {
    String url = Constant.INVALID_LOGIN_URL;
    if (isValid) {
      url = Constant.LOGIN_URL;
    }
    System.out.println("Request     : " + url);
    unitDriver.get(url);
    System.out.println("Response    :" + unitDriver.getPageSource());
    return unitDriver;
  }

  protected void logOut() {
    unitDriver.close();
    setUp();
  }
}
