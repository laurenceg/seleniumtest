package com.zenoradio.api.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.zenoradio.api.Constant;

import junit.framework.Assert;

public class LoginTest extends Base {


  @Test
  public void testInvalidLogin() {
    System.out.println(
        ">When user logs in using Invalid Usernameand Password. Then login should  be Unsuccessful.");

    // GIVEN
    login(false).getPageSource();
    // WHEN
    String loginJsonResponse = getHtmlUnitDriver().getPageSource();

    // THEN
    Assert.assertNotNull(loginJsonResponse);
    assertEquals(true, loginJsonResponse.contains(Constant.IS_STAT_FAIL));
    assertEquals(true, loginJsonResponse.contains(Constant.IS_KEY_INVALID_LOGIN));
    System.out.println();

  }


  @Test
  public void testValidLogin() {
    System.out.println(
        ">When user logs in using valid Username and Password. Then login should be successful.");
    // GIVEN
    login(true).getPageSource();
    // WHEN
    String loginJsonResponse = getHtmlUnitDriver().getPageSource();

    // THEN
    Assert.assertNotNull(loginJsonResponse);
    assertEquals(true, loginJsonResponse.contains(Constant.STAT_OK));
    assertEquals(false, loginJsonResponse.contains(Constant.IS_KEY_INVALID_LOGIN));
    System.out.println();

  }


}
