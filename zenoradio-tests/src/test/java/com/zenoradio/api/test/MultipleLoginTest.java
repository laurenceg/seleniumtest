package com.zenoradio.api.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.junit.Test;

import com.zenoradio.api.Constant;

import junit.framework.Assert;

public class MultipleLoginTest extends Base {

  @Test
  public void testValidMultipleLogin()
      throws ClientProtocolException, IOException, InterruptedException {
    System.out.println(">Login - Logout script: Loop " + Constant.REPEAT_COUNT_FIVE + "times");
    // GIVEN
    // WHEN
    for (int i = 0; i < Constant.REPEAT_COUNT_FIVE; i++) {
      String loginJsonResponse = login(true).getPageSource();

      // THEN
      Assert.assertNotNull(loginJsonResponse);
      assertEquals(true, loginJsonResponse.contains(Constant.STAT_OK));
      assertEquals(false, loginJsonResponse.contains(Constant.IS_KEY_INVALID_LOGIN));

      Thread.sleep(Constant.TEN_SEC_DELAY);
      logOut();
      Thread.sleep(Constant.TEN_SEC_DELAY);

    }
  }

}
