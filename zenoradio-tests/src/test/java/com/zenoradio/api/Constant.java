package com.zenoradio.api;

public final class Constant {
  public static String HOST = "https://stage-rtapi.zenoradio.com";
  public static String USER_NAME = "laurence@mailinator.com";
  public static String PW = "otsootso";
  public static String CONTENT_ID = "13785";
  //public static String HOST = "https://dev-rtapi.zenoradio.com";
  //public static String USER_NAME = "user-2@zenoradio.com";
  //public static String PW = "nuptiasinrubo";
  //public static String CONTENT_ID = "15";
  public static String INVALID_USER_NAME = "!NvAliD@zenoradio.com";
  public static String INVALID_PW = "!NvAliD";

  public static final String LOGIN_URL =
      HOST + "/api/Login/create?data={\"handler\":\"broadcaster\",\"email\":\"" + USER_NAME
          + "\",\"password\":\"" + PW + "\"}";

  public static final String INVALID_LOGIN_URL =
      HOST + "/api/Login/create?data={\"handler\":\"broadcaster\",\"email\":\"" + INVALID_USER_NAME
          + "\",\"password\":\"" + INVALID_PW + "\"}";

  public static final String ON_AIR_URL =
      HOST + "/api/DataContent/update?data={\"id\": "+CONTENT_ID+", \"onAir\": true}";

  public static final String OFF_AIR_URL =
      HOST + "/api/DataContent/update?data={\"id\": "+CONTENT_ID+", \"onAir\": false}";

  public static final String INVALID_LOGIN = "INVALID_LOGIN";

  public static final String STAT_OK = "ok";
  public static final String STAT_FAIL = "fail";

  public static final String IS_STAT_OK = "\"stat\":\"ok\"";
  public static final String IS_STAT_FAIL = "\"stat\":\"fail\"";
  public static final String IS_ON_AIR_STR = "\"onAir\":true";
  public static final String IS_OFF_AIR_STR = "\"onAir\":false";
  public static final String IS_KEY_INVALID_LOGIN = "\"key\":\"INVALID_LOGIN\"";
  public static final String IS_NOT_AUTHORIZED="\"key\":\"NOT_AUTHRORIZED\"";  
  public static final int TEN_SEC_DELAY = 10 * 1000;
  public static final int ONE_SEC_DELAY = 1 * 1000;
  public static final int REPEAT_COUNT_FIVE = 5;
  public static final int REPEAT_COUNT_1K = 1000;
}
