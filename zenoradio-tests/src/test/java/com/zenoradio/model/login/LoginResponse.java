package com.zenoradio.model.login;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "response")
public class LoginResponse {
  private String stat;
  private Data data;


  @XmlElement(name = "stat")
  public String getStat() {
    return stat;
  }

  public void setStat(String stat) {
    this.stat = stat;
  }

  @XmlElement(name = "data")
  public Data getData() {
    return data;
  }

  public void setData(Data data) {
    this.data = data;
  }

}
