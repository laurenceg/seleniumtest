package com.zenoradio.model.login;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "data")
public class Data {
  private String handler;
  private String token;
  private String webSessionId;
  private String userId;
  private List<String> contents;
  private String key;
  private String message;

  @XmlElement(name = "handler")
  public String getHandler() {
    return handler;
  }

  public void setHandler(String handler) {
    this.handler = handler;
  }

  @XmlElement(name = "token")
  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  @XmlElement(name = "webSessionId")
  public String getWebSessionId() {
    return webSessionId;
  }

  public void setWebSessionId(String webSessionId) {
    this.webSessionId = webSessionId;
  }

  @XmlElement(name = "userId")
  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  @XmlElement(name = "contents")
  public List<String> getContents() {
    return contents;
  }

  public void setContents(List<String> contents) {
    this.contents = contents;
  }

  @XmlElement(name = "key")
  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  @XmlElement(name = "message")
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
