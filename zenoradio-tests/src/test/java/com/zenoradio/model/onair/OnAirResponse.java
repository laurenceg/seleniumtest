package com.zenoradio.model.onair;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "onair_response")
public class OnAirResponse {
  private String stat;
  private OnAirData onAirData;

  @XmlElement(name = "stat")
  public String getStat() {
    return stat;
  }

  public void setStat(String stat) {
    this.stat = stat;
  }

  @XmlElement(name = "data")
  public OnAirData getOnAirData() {
    return onAirData;
  }

  public void setOnAirData(OnAirData onAirData) {
    this.onAirData = onAirData;
  }
}
