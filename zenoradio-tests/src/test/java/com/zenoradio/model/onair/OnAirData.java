package com.zenoradio.model.onair;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "data")
public class OnAirData {
  private Integer id;
  private boolean onAir;
  private String key;
  private String message;
  private OnAirMedia onAirMedia;



  @XmlElement(name = "id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  @XmlElement(name = "onAir")
  public boolean isOnAir() {
    return onAir;
  }

  public void setOnAir(boolean onAir) {
    this.onAir = onAir;
  }

  @XmlElement(name = "key")
  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  @XmlElement(name = "message")
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @XmlElement(name = "onAirMedia")
  public OnAirMedia getOnAirMedia() {
    return onAirMedia;
  }

  public void setOnAirMedia(OnAirMedia onAirMedia) {
    this.onAirMedia = onAirMedia;
  }

}
