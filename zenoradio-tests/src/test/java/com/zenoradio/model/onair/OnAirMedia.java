package com.zenoradio.model.onair;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "media")
public class OnAirMedia {
  private boolean disabled;

  @XmlElement(name = "disabled")
  public boolean isDisabled() {
    return disabled;
  }

  public void setDisabled(boolean disabled) {
    this.disabled = disabled;
  }
}
